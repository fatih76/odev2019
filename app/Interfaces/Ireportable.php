<?php
namespace VendingMachine\Interfaces;

interface iReportable
{
    public function reportCapacity() : string ;
}
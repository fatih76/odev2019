<?php
namespace VendingMachine\Interfaces;

use VendingMachine\Classes\Drinks\DrinkAbstract;

interface iContainer
{
    public function isFull() : bool;
    public function isEmpty() : bool;
    public function retriveDrink(DrinkAbstract $drink) : bool;
    public function storeDrink(DrinkAbstract $drink) : bool;
}
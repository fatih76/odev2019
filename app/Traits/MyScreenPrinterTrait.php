<?php
namespace VendingMachine\Traits;

trait MyScreenPrinterTrait
{
    public function printWithBeauty($message='', $with_asterix = true) :  string
    {
        if(empty($message))
            return ' ';

        if(!$with_asterix)
            return $message;

        $str ="\n\n\n";
        $str .=  str_repeat("*", 45);
        $str .=  "\n****\t{$message}\t****\n";
        $str .=  str_repeat("*", 45);

        return $str;
    }

}
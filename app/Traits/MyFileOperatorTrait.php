<?php
namespace VendingMachine\Traits;

trait MyFileOperatorTrait
{

    private function getDataFromFile($file_path='') : string
    {
        if(!file_exists($file_path))
        {
            if(file_put_contents($file_path, serialize([]) ) === FALSE)
                throw new Exception("Registry klasorunde yazma hakkı yok, path: {$file_path}");

        }
        return (file_get_contents($file_path));

    }

    private function setDataIntoFile($file_path='', $new_value='') : bool
    {
        return (file_put_contents($file_path, ($new_value)) !== FALSE);
    }

}

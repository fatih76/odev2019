<?php
namespace VendingMachine\Classes;

use Exception;
use VendingMachine\Classes\Drinks\DrinkAbstract;
use VendingMachine\Interfaces\iReportable;
use VendingMachine\Traits\MyScreenPrinterTrait;

class Cabinet implements iReportable
{
    use MyScreenPrinterTrait;

    private $door, $shelf_manager;

    private static $is_busy;    // makine bir işlem yapıyor iken, $is_busy true hale geçer ve yeni veya farklı bir işlem yapılmasını engeller.
    private $capacity_status;   // private degişken olmasına rağmen, değer atamalarını ShelfManager aşağıdaki _set() magic metodu sayesinde yapabiliyor.

    //kabul edilen capacity durumları - gerçek bir projede bu kısım ayrı bir class olup, durumu ve toplam kaç icecek depolandıgı gibi bilgiler içerisinde tutulabilir. istenilen ödevin tam kapsamını bilmediğimden o kadar küçük parçalara bölmedim.
    const CAPACITY_STATUS_EMPTY      = 'empty';
    const CAPACITY_STATUS_SEMIFULL   = 'semi-full';
    const CAPACITY_STATUS_FULL       = 'full';

    public function __construct()
    {
        self::$is_busy  = false;
        $this->door     = CabinetDoor::getInstance();
        $this->shelf_manager    = new ShelfManager($this);
    }

    public function __set($property, $value) {
        if ($property=="capacity_status")
        {
            $this->$property = $value;
        }
    }

    public function operate($operation_type, DrinkAbstract $drink)
    {

        try {

            //kontrol - makine hali hazırda kullanılıyor mu?
            if(self::$is_busy)
                throw new Exception('Machine is busy, try again later');

            //makineyi meşgul moda geçirelim
            self::$is_busy = true;

            //önce dolabın kapagını açalım. yapamazsa uygun mesajlı bir exception atılacaktır
            $this->door->operate(CabinetDoor::DOOR_STATUS_OPEN);

            //buraya gelebildi ise, herşey yolunda, cabinet kapağı açıldı, gelen istek tipine göre işlemlere devam:
            switch ($operation_type)
            {
                case WORKING_MODE_RETRIVE_DRINK:
                    //kapasite kontolü-makine de içecek bitmiş ise, ShelfManager en son içecegini verdikten sonra buradaki durumu empty'e çekmiştir. güncel makine durumunu kontrol edelim:
                    if($this->capacity_status == self::CAPACITY_STATUS_EMPTY)
                        throw new Exception("Machine is EMPTY, can not serve anymore drinks");

                    //shelf_manager'a bana bir adet icecek vermesini isteyelim, yapamazsa exception atacak. shelfmanager gerekiyorsa Cabinet'in capacity_status degerini günceller.
                    $this->shelf_manager->retriveDrinkFromShelf($drink);

                break;

                case WORKING_MODE_STORE_DRINK:
                    //kapasite kontolü-makine de içecek koyacak yer kalmamış ise, ShelfManager en son içecegini yerleştirdikten sonra buradaki durumu FULL'e çekmiştir. güncel makine durumunu kontrol edelim:
                    if($this->capacity_status == self::CAPACITY_STATUS_FULL)
                        throw new Exception("Machine is FULL, can not store anymore drinks");

                    //shelf_manager'a bana bir adet icecek depolamasını isteyelim, yapamazsa exception atacak
                    $this->shelf_manager->storeDrinkToShelf($drink);

                break;

                default:
                    throw new Exception('Undefined / UnAuth. Request Type, Bailing Out!');
                break;
            }

            //tüm işlemler tamam. güvenli kapatma işlemleri için finally kısmına bakınız.

        } catch (Exception $e) {
            //işlemler sırasında bir sıkıntı var ise uygun bir uyarı mesajı exceptiondan gelecektir, bu uyarı mesajını ekrana gösterelim
            echo $this->printWithBeauty( "WARNING: Houston we've a Problem ! ".$e->getMessage());
        }
        finally {
            //güvenli kapatma işlemleri başlıyor:

            //1-makine kapağı açık ise, kapatılmalı
            if($this->door->getCurrentDoorStatus() == CabinetDoor::DOOR_STATUS_OPEN)
                $this->door->operate(CabinetDoor::DOOR_STATUS_CLOSE);

            //2-makine artık yeni bir isteği karşılayabilir duruma gelsin:
            self::$is_busy = false;
        }
        //tüm işlemler bitti
        return true;
    }


    public function reportCapacity() : string
    {
        $shelf_report = $this->shelf_manager->getShelfCapacityStatusReport();
        $report = <<<STR


        Cabinet Capacity:\t {$this->capacity_status}
        
        Shelf Status: \t {$shelf_report} 
        
STR;
        return $this->printWithBeauty($report, false);
    }

}
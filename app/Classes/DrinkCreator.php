<?php
namespace VendingMachine\Classes;

use VendingMachine\Classes\Drinks\DrinkAbstract;

use Exception;

class DrinkCreator
{

    public function setDrink($drink_type='')  : DrinkAbstract
    {

        $drink_type = $this->validatedName($drink_type);
        if(empty($drink_type))
            throw new Exception('Bu işlem için, bir içecek tipi belirtmelisiniz');

        $drink_class = '\VendingMachine\Classes\Drinks\Drink'.$drink_type;

        //tanımlanmış bir içecek mi istiyor? makineden uzay gemisi falan istemesin?
        if( ! class_exists($drink_class) )
            throw new Exception('Undefined Drink Type!');

        //makineye uygun bir içecek mi  (mesela Whiskey, bu makine için uygun bir içecek tipi değil)
        if( ! in_array('VendingMachine\Interfaces\iStorableDrink', class_implements($drink_class)) )
            throw new Exception( $drink_type. ', Not Compatible Drink Type! ');

        //istenilen içecek uygun, istenilen tip icecek class'indan bir instance oluşturup, verelim:
        return new $drink_class();
    }

    //sembolik bir guvenlik kontrolu - kullanicidan gelen inputu sanitize etmek icin:
    private function validatedName($str='') {
        if (!preg_match('/[^A-Za-z0-9]/', $str)) {
            return ucwords(strtolower($str));
        }
        return '';
    }

}
<?php
namespace VendingMachine\Classes;

use Exception;
use VendingMachine\Classes\Drinks\DrinkAbstract;
use VendingMachine\Traits\MyScreenPrinterTrait;

class ShelfManager
{
    use MyScreenPrinterTrait;
    const SHELF_CAPACITY = 3;  //makinenin, toplam kaç adet raf desteği olduğunu buradan ayarlayabiliyoruz.

    private $shelves, $cabinet;

    public function __construct(Cabinet $cabinet)
    {
        $this->cabinet = $cabinet;
        $this->shelves = [];
        $this->initialize();
    }

    public function initialize()
    {
        if(empty($this->shelves))
        {
            //ilk kullanım için, makinenin 3 adet rafını yönetmek amacıyla kaydedelim:
            for($i=1; $i<=self::SHELF_CAPACITY; $i++)
            {
                $this->addShelf(CabinetShelf::registerShelf($i));
            }

            //set Cabinet Capacity Status for the first-time use
            $this->updateMachineCapacity();
        }
    }

    public function storeDrinkToShelf(DrinkAbstract $drink) {
        //içeceği koyacak uygun bir raf arayalım, eğer tamamen dolu bir raf var ise, o raf bilgisi gelecek.
        if(!$shelf = $this->getFirstAvailableShelf($drink))
            throw new Exception("No more desi capacity left for this type of drink");

        //buraya gelmiş ise, uygun bir raf bulundu demek, rafın içine içececeğimizi koyalım.
        if(!$shelf->storeDrink($drink))
            throw new Exception("Couldnt store on the shelf - Internal Error");

        //exception yok ise, işlem başarılı. ekrana yaz:
        echo $this->printWithBeauty("DRINK {$drink->reportNameAndSize()} IS SUCCESFULLY STORED !");

        $this->updateMachineCapacity();

        return true;
    }

    public function retriveDrinkFromShelf(DrinkAbstract $drink)
    {
        //içeceği alacak bir raf arayalım, içi boş olan ilk raf gelmeli
        if(!$shelf = $this->getFirstNonEmptyShelf($drink))
            throw new Exception(" {$drink->reportNameAndSize()} Is Out Of Stock :( ");

        //buraya gelmiş ise, uygun bir raf bulundu demek, rafın içine içececeğimizi koyalım.
        if(!$shelf->retriveDrink($drink))
            throw new Exception("Couldnt retrive from the shelf - Internal Error");

        //exception yok ise, işlem başarılı. ekrana yaz:
        echo $this->printWithBeauty( "DRINK {$drink->reportNameAndSize()} READY, YOU CAN TAKE IT !");

        $this->updateMachineCapacity();

        return true;
    }

    public function getShelfCapacityStatusReport()
    {
        $report = '';

        foreach($this->shelves AS $shelf) {
            $report .= $shelf->reportCapacity();
        }

        return $report;
    }

    private function getFirstNonEmptyShelf(DrinkAbstract $drink)
    {
        foreach ($this->shelves AS $shelf)
        {
            if($shelf->hasDrink($drink))
               return $shelf;
        }

        return false;
    }

    private function getFirstAvailableShelf(DrinkAbstract $drink)
    {
        foreach ($this->shelves AS $shelf)
        {
            if( ! $shelf->hasStorageFor($drink) )
                continue;

            return $shelf;
        }
        return false;
    }

    private function updateMachineCapacity() {
        $total_storage_left = 0;
        foreach($this->shelves AS $shelf) {
            $total_storage_left += $shelf->getCurrentCapacityLeft();
        }

        if(empty($total_storage_left))
            $this->cabinet->capacity_status = Cabinet::CAPACITY_STATUS_FULL;
        else if($total_storage_left == self::SHELF_CAPACITY * CabinetShelf::SHELF_CAPACITY)
            $this->cabinet->capacity_status = Cabinet::CAPACITY_STATUS_EMPTY;
        else
            $this->cabinet->capacity_status = Cabinet::CAPACITY_STATUS_SEMIFULL;
    }

    private function addShelf(CabinetShelf $shelf)
    {
        array_push($this->shelves, $shelf);
    }

}
<?php
namespace VendingMachine\Classes;

use Exception;
use VendingMachine\Classes\Drinks\DrinkAbstract;
use VendingMachine\Traits\MyFileOperatorTrait;
use VendingMachine\Interfaces\{iReportable,iContainer};


class CabinetShelf implements iReportable,iContainer
{

    use MyFileOperatorTrait;

    const SHELF_CAPACITY  = 20;

    private $stored_drinks;
    private $shelf_name, $registry;

    private function __construct($shelf_name='')
    {
        $this->shelf_name           = 'SHELF_NUMBER_'.$shelf_name;
        $this->registry             = MACHINE_ROOT_DIR.'Registry/'.$this->shelf_name;
        $this->stored_drinks        = $this->getStoredDrinksFromDatabase();
    }

    //named constructor
    public static function registerShelf($id=0)
    {
        if(empty($id))
            $id = uniqid();
        return new CabinetShelf($id);
    }

    /**
     * Get one drink from shelf
     * @throws Exception
     */
    public function retriveDrink(DrinkAbstract $drink)  : bool
    {
        if(empty($this->stored_drinks))
            throw new Exception("Shelf Is Empty", 9998);

        if (($key = array_search($drink, $this->stored_drinks)) !== false) {
            unset($this->stored_drinks[$key]);
        }
        $this->updateCapacity();

        return true;
    }

    /**
     * Store one drink on the shelf
     * @throws Exception
     */
    public function storeDrink(DrinkAbstract $drink) : bool
    {
        if(! $this->hasStorageFor($drink) )
            throw new Exception("Shelf Capacity Exceeded", 9999);

        array_push($this->stored_drinks, $drink);
        $this->updateCapacity();

        return true;
    }

    public function reportCapacity() :string
    {
        return "\n\t{$this->shelf_name} capacity : {$this->getCurrentFilledCapacity()} / " . self::SHELF_CAPACITY;

    }

    public function getCurrentFilledCapacity() : int {
        $desi = 0;
        foreach($this->stored_drinks AS $drink)
        {
            $desi += $this->getDesi($drink);
        }
        return $desi;
    }

    public function getCurrentCapacityLeft() : int {
        return (self::SHELF_CAPACITY - $this->getCurrentFilledCapacity() );
    }

    /**
     * shelf capacity state tracker
     * @param string $operation
     */
    private function updateCapacity()
    {
        $this->saveStoredDrinksIntoDatabase();
        $this->getStoredDrinksFromDatabase();
    }

    public function hasDrink($drink) : bool {
        return in_array($drink, $this->stored_drinks);
    }

    public function hasStorageFor($drink) : bool {
        return ( ($this->getCurrentCapacityLeft() - $this->getDesi($drink) ) >= 0 );
    }

    public function isFull() : bool
    {
        return ( $this->getCurrentCapacityLeft() == 0 );
    }

    public function isEmpty() : bool
    {
        return ( ! empty( $this->stored_drinks ) ) ;
    }

    //içecek, Raf üzerinde, kaç birimlik yer kaplıyor?
    private function getDesi(DrinkAbstract $drink) {

        return (int) ceil(  $drink->getDimensions() );      //my global,ultimate desi-calculation formula  :)
        //desi formulu: iceceğin genişlik ve yüksekliğine göre hacim hesaplıyor. bunu bir üst rakama yuvarlıyor.
        // icecek hacmi 1.3 ise rafda 2 birim yer kaplıyor, 0.9 ise 1 birim kaplıyor gibi.
        // sembolik bir desi hesabını bu şekilde ayarladım.
    }

    private function getStoredDrinksFromDatabase()
    {
        //raf icerisinde bulunan tum icecekleri parse edip rafin en son state ini hatirlayalim
        $stored_drinks = unserialize($this->getDataFromFile($this->registry));
        return $stored_drinks;
    }

    private function saveStoredDrinksIntoDatabase()
    {
        return ($this->setDataIntoFile($this->registry, serialize($this->stored_drinks) ) !== FALSE);
    }

}
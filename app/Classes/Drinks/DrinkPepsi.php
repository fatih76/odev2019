<?php
namespace VendingMachine\Classes\Drinks;

use VendingMachine\Interfaces\iStorableDrink;

class DrinkPepsi extends DrinkAbstract implements iStorableDrink
{

    public function __construct() {
        parent::__construct();
    }

    protected function setBrand() : DrinkAbstract {
        $this->brand = 'Pepsi';
        return $this;
    }

    protected function setVolume(): DrinkAbstract {
        $this->volume = 33;
        return $this;
    }

    protected function setBoxDimensions(): DrinkAbstract {
        $this->box_height = 1;
        $this->box_width  = 1;
        return $this;
    }

}
<?php
namespace VendingMachine\Classes\Drinks;

use VendingMachine\Interfaces\iStorableDrink;

class DrinkCoke50 extends DrinkCoke  implements iStorableDrink
{

    public function __construct() {
        parent::__construct();
    }

    protected function setVolume(): DrinkAbstract {
        $this->volume = 50;
        return $this;
    }

    protected function setBoxDimensions(): DrinkAbstract {
        $this->box_height = 1.5;
        $this->box_width  = 1.2;
        return $this;
    }

}
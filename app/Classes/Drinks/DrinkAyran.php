<?php
namespace VendingMachine\Classes\Drinks;


class DrinkAyran extends DrinkAbstract // <- ayran  iStorableDrink interface'ini implement etmiyor. Bu makine için uygun içecek degil demek!
{
    public function __construct() {
        parent::__construct();
    }

    protected function setBrand() : DrinkAbstract {
        $this->brand = 'Ayran';
        return $this;
    }

    protected function setVolume(): DrinkAbstract {
        $this->volume = 33;
        return $this;
    }

    protected function setBoxDimensions(): DrinkAbstract {
        $this->box_height = 0.75;
        $this->box_width  = 1;
        return $this;
    }
    
}
<?php
namespace VendingMachine\Classes\Drinks;


abstract class DrinkAbstract
{

    protected $brand, $volume, $box_width, $box_height;

    public function __construct()
    {
        $this->setDrinkProperties();
    }

    abstract protected function setBrand() :  DrinkAbstract;
    abstract protected function setVolume() : DrinkAbstract;
    abstract protected function setBoxDimensions() : DrinkAbstract;

    //içecek özeliklerini atansın
    protected function setDrinkProperties()
    {
        $this->setBrand()
            ->setVolume()
            ->setBoxDimensions();
    }

    //içeceğin boyutları
    public function getDimensions() : float
    {
        return  $this->box_height*$this->box_width;
    }

    public function reportNameAndSize() {
        return " {$this->brand}, {$this->volume} Cl. ";
    }

}
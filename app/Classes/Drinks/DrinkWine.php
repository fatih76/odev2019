<?php
namespace VendingMachine\Classes\Drinks;


class DrinkWine extends DrinkAbstract // <- Şarap iStorableDrink interface'ini implement etmiyor. Bu makine için uygun içecek degil demek!
{
    public function __construct() {
        parent::__construct();
    }

    protected function setBrand() : DrinkAbstract {
        $this->brand = 'Şarap';
        return $this;
    }

    protected function setVolume(): DrinkAbstract {
        $this->volume = 1000;
        return $this;
    }

    protected function setBoxDimensions(): DrinkAbstract {
        $this->box_height = 3;
        $this->box_width  = 1;
        return $this;
    }
    
}
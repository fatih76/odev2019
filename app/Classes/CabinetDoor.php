<?php
namespace VendingMachine\Classes;

use Exception;

final class CabinetDoor
{

    private static $instance;
    private static $door_status;

    const DOOR_STATUS_CLOSE = 'Closed';
    const DOOR_STATUS_OPEN  = 'Opened';

    private function __construct()
    {
    }

    /**
     * make singleton Cabinet Door
     *
     * @return CabinetDoor
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new CabinetDoor();
        }
        return static::$instance;
    }


    /**
     * Open or Close the CabinetDoor
     * @param string $new_status
     * @return bool
     */
    public static function operate($new_status)
    {
        //geçerli bir komut gönderilmemiş ise, isteği kabul etme!
        if(empty($new_status) || !in_array($new_status, [self::DOOR_STATUS_CLOSE,self::DOOR_STATUS_OPEN]))
            throw new Exception("Bad Request");

        //kapı hala açık ise, yeniden açık komutu gelmemeli, veya hali hazırda kapalı ise kapat komutu gelmemeli!
        if($new_status == static::$door_status)
            throw new Exception("Door is Already ". $new_status);

        //herhangi bir sorun yok ise, işlemi gerçekleştir:
        static::$door_status = $new_status;

        return true;
    }

    /**
     * Get current status of cabinet door
     * @return mixed
     */
    public function getCurrentDoorStatus()
    {
        return self::$door_status;
    }

}
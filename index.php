<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/vendor/autoload.php';

use VendingMachine\Classes\Cabinet;
use VendingMachine\Classes\DrinkCreator;

const MACHINE_ROOT_DIR = __DIR__ . '/';

//izin verilen çalışma modları:
const WORKING_MODE_STORE_DRINK   = 'store';
const WORKING_MODE_RETRIVE_DRINK = 'retrive';

$drink_requested = $command = null;
$vending_machine = new Cabinet();

try
{
    (
        function($argv) use ( &$command  ) {
            $command =  (!empty($argv[1]) && in_array($argv[1],['take', 'store']))
                ? ( ($argv[1] == 'take') ? WORKING_MODE_RETRIVE_DRINK : WORKING_MODE_STORE_DRINK  )
                : $argv[1] ?? 'help';
            return true;
        }
    )( $argv ) ;

    switch ($command)
    {
        case WORKING_MODE_STORE_DRINK:
        case WORKING_MODE_RETRIVE_DRINK:

            //istenen icecek tipi - bir object instance olusturalim (exception gelirse istek/icecek_tipi hatalidir):
            $drink_requested =
                ( new DrinkCreator )->setDrink (
                    ($argv[2] ?? null)
                );

            $vending_machine->operate (
                $command,
                $drink_requested
            );
            echo $vending_machine->reportCapacity();
            break;

        case 'status':
            echo $vending_machine->reportCapacity();
            break;

        case 'help':
        default:
            printUsage();
            break;
    }

} catch(Exception $e)
{

    new class($e->getMessage()) {

        use VendingMachine\Traits\MyScreenPrinterTrait;

        public function __construct(string $input)
        {
            echo $this->printWithBeauty( "WARNING: Houston we've a Problem ! ".$input);

        }
    };
}







//---





function printUsage()
{
    echo <<<STR
    
    
*********************************************************
*                                                       *
*  Hi, I'm a Vending Machine.                           *
*                                                       *
*  Available Commands are:                              *
*                                                       *
*   take  : to take/fetch 1 drink from machine          *
*   store : to store 1 drink into the machine           *
*   status: get current machine & shelf capacity status *
*   help: : to get this mini-help usage                 *
*                                                       *
*                                                       *
*   e.g:                                                *
*    to execute this script use these commands below:   *
*                                                       *
*         php index.php take coke                       *
*         php index.php take coke50                     *
*         php index.php store coke                      *
*         php index.php store coke50                    *
*         php index.php status                          *
*                                                       *
*********************************************************

        
STR;

}


echo "\n\n";

